CREATE TABLE application_user (
  id              BIGSERIAL PRIMARY KEY,
  auth_id         TEXT UNIQUE NOT NULL,
  family_name     TEXT        NOT NULL,
  given_name      TEXT        NOT NULL,
  profile_picture TEXT        NOT NULL
);

CREATE TABLE conversation (
  id         BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

CREATE TABLE user_conversation (
  user_id         BIGINT NOT NULL REFERENCES application_user (id) ON DELETE CASCADE,
  conversation_id BIGINT NOT NULL REFERENCES conversation (id) ON DELETE CASCADE
);

CREATE TABLE message (
  id              BIGSERIAL PRIMARY KEY,
  conversation_id BIGINT                      NOT NULL REFERENCES conversation (id) ON DELETE CASCADE,
  user_id         BIGINT                      NOT NULL REFERENCES application_user (id) ON DELETE CASCADE,
  created_at      TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  content         TEXT                        NOT NULL
);

CREATE TABLE friendship (
  user1_id   BIGINT REFERENCES application_user (id) ON DELETE CASCADE,
  user2_id   BIGINT REFERENCES application_user (id) ON DELETE CASCADE,
  created_at TIMESTAMP NOT NULL,
  PRIMARY KEY (user1_id, user2_id)
);