# LatteTalk
Chat app using Golang and WebSocket.

### TODO
- [x] Remove debug logs.
- [x] Handle WebSocket upgrade concurrently.
- [ ] Use customize Reader and Writer for WebSocket connection.

